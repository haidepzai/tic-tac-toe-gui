package de.hdm_stuttgart.mi.sd;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;

public class Main {

    private static final Logger log = LogManager.getLogger(Main.class);

    protected static JFrame frame = new JFrame("Tic Tac Toe by Hai");

    public static void main(String[] args)
    {
        log.info("Program start");

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(new TicTacToeGUI());
        frame.setBounds(600,200,600,600);
        frame.setVisible(true);
    }

}
