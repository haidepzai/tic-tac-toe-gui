package de.hdm_stuttgart.mi.sd;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;

public class TicTacToeGUI extends JPanel {

    private static final Logger log = LogManager.getLogger(TicTacToeGUI.class);

    private JMenuBar menuBar;
    private JMenu menu;

    protected static JButton buttons[] = new JButton[9]; //Erstellen von 9 Buttons als Array
    protected static int playersTurn = 0; //Ungerade = Player X; Gerade = Player O;
    protected static boolean isFull = false; //Für den Fall Untentschieden - tritt nur ein, wenn alle Buttons geklickt worden

    private static JButton exitButton = new JButton();
    private static JButton resetButton = new JButton();
    private static JLabel textField = new JLabel("<html>By Hai © <br> 2020</html>");

    public TicTacToeGUI()
    {
        log.debug("Menu initialized");
        setLayout(new GridLayout(4,3)); //3x3 Grid
        initializebuttons();
        initializeMenuBar();
        initializeExit();
        initializeReset();
        initializeLabel();

    }

    private void initializeMenuBar(){
        menuBar = new JMenuBar();
        menu = new JMenu("Menü");
    }

    /**
     * Initialisierung der 9 Buttons
     * @value buttonCounter für Logger
     */
    private void initializebuttons() {

        int buttonCounter = 0;

        for(int i = 0; i <= 8; i++)
        {
            buttons[i] = new JButton();
            buttons[i].setText("");
            buttons[i].addActionListener(new ButtonListener());
            buttons[i].setBackground(Color.getHSBColor(159,44,46));
            buttons[i].setBorder(BorderFactory.createLineBorder(Color.ORANGE, 4));

            add(buttons[i]); //Buttons in JPanel hinzufügen
            buttonCounter++;
        }
        log.debug(buttonCounter + " Buttons added");
    }
    //Exit Button initialisieren
    private void initializeExit(){
        exitButton.setText("Exit");
        exitButton.addActionListener(e -> Main.frame.dispose()); //Close Window
        exitButton.setBorder(BorderFactory.createLineBorder(Color.ORANGE, 4));
        exitButton.setFont(new Font("Tahoma", Font.BOLD, 20));
        exitButton.setForeground(Color.RED);
        add(exitButton); //Ins GridLayout hinzufügen
    }
    //Reset Button initialisieren
    private void initializeReset(){
        resetButton.setText("Reset");
        resetButton.addActionListener(e -> resetButtons()); //Ruft Methode resetButtons onclick auf
        resetButton.setBorder(BorderFactory.createLineBorder(Color.ORANGE, 4));
        resetButton.setFont(new Font("Tahoma", Font.BOLD, 20));
        resetButton.setForeground(Color.BLUE);
        add(resetButton); //Ins GridLayout hinzufügen
    }
    //Hai ©
    private void initializeLabel(){

        textField.setHorizontalAlignment(SwingConstants.CENTER);
        textField.setFont(new Font("Lucida Handwriting", Font.BOLD | Font.ITALIC, 24));
        textField.setBounds(82, 25, 355, 161);
        textField.setBorder(BorderFactory.createLineBorder(Color.ORANGE, 4));
        add(textField); //Ins GridLayout hinzufügen

    }
    //Spielfeld resetten
    public static void resetButtons()
    {
        for(int i = 0; i <= 8; i++)
        {
            buttons[i].setText("");
        }
        //Ausgangswerte resetten nach dem Spiel
        playersTurn = 0;
        isFull = false;
        log.debug("Field reset");
        log.debug("New Game initialized");
    }

    /**
     *  Überprüfen, ob alle Buttons geklickt sind (Für den Fall Untentschieden)
     *  Tritt erst ein, wenn alle Spieler geklickt haben bzw wenn das Feld voll ist
     *  @value isFull = true
     */
    public static void checkButtons(){
        if(playersTurn == 9) { //Tretet erst in der 9. Runde ein
            for (JButton button : buttons) {
                if (button == null) {
                    isFull = false;
                    break;
                } else {
                    isFull = true;
                    log.trace("All fields clicked: " + isFull);
                }
            }
        }
    }

}