package de.hdm_stuttgart.mi.sd;

public class IllegalTurnException extends Exception {

    public IllegalTurnException(String message){
        super(message);
    }

}
