package de.hdm_stuttgart.mi.sd;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonListener implements ActionListener {

    private static final Logger log = LogManager.getLogger(ButtonListener.class);

    public void actionPerformed(ActionEvent e){

                                //Typecasting JButton
        JButton buttonClicked = (JButton)e.getSource(); //e.getSource() = der angeklickte Button

        //Stellt sicher, dass nur leere Felder angeklickt werden können
        if(((JButton)e.getSource()).getText().equals("") && checkForWin() == false){

            //playersTurn % 2 == 0 -> Spieler X an der Reihe
            if(TicTacToeGUI.playersTurn % 2 == 0) {
                buttonClicked.setText("X");
                buttonClicked.setForeground(Color.RED);
                buttonClicked.setFont(new Font("Tahoma", Font.BOLD, 76));
                TicTacToeGUI.checkButtons();
                log.trace("Spieler X an der Reihe");
            }
            else {
                buttonClicked.setText("O");
                buttonClicked.setForeground(Color.BLUE);
                buttonClicked.setFont(new Font("Tahoma", Font.BOLD, 76));
                TicTacToeGUI.checkButtons();
                log.trace("Spieler O an der Reihe");
            }

            //Sieger bestimmen (Wenn er am Zug ist und 3 in einer Reihe hat)
            if(checkForWin() == true && TicTacToeGUI.playersTurn %2 == 0){
                JOptionPane.showMessageDialog(null, "Spieler X hat gewonnen", "Herzlichen Glückwunsch", JOptionPane.OK_OPTION);
                TicTacToeGUI.resetButtons();
                log.info("Spieler X hat gewonnen");
            } else if (checkForWin() == true && TicTacToeGUI.playersTurn %2 == 1){
                JOptionPane.showMessageDialog(null, "Spieler O hat gewonnen", "Herzlichen Glückwunsch", JOptionPane.OK_OPTION);
                TicTacToeGUI.resetButtons();
                log.info("Spieler Y hat gewonnen");
            } else if (checkForWin() == false && TicTacToeGUI.isFull){
                JOptionPane.showMessageDialog(null, "Untentschieden", "Kein Gewinner", JOptionPane.OK_OPTION);
                TicTacToeGUI.resetButtons();
                log.info("Untentschieden");
            }

            TicTacToeGUI.playersTurn++;
        } else {
            try {
                throw new IllegalTurnException("Ungültiger Spielzug");
            } catch (IllegalTurnException ex) {
                ex.printStackTrace();
            }
            JOptionPane.showMessageDialog(null, "Ungültiger Zug", "Fehler", JOptionPane.ERROR_MESSAGE);
            log.error("Ungültiger Spielzug");
        }


    }

    //Sieg überprüfen
    private boolean checkForWin()
    {
        /**   Buttons sind nach diesem Muster aufgebaut [9] Array:
         *      0 | 1 | 2
         *      3 | 4 | 5
         *      6 | 7 | 8
         */
        //Horizontal
        if(checkLine(0,1) && checkLine(1,2)) {
            return true;
        }
        else if(checkLine(3,4) && checkLine(4,5)) {
            return true;
        }
        else if (checkLine(6,7) && checkLine(7,8)) {
            return true;
        }

            //Vertikal
        else if (checkLine(0,3) && checkLine(3,6)) {
            return true;
        }
        else if (checkLine(1,4) && checkLine(4,7)) {
            return true;
        }
        else if  (checkLine(2,5) && checkLine(5,8)) {
            return true;
        }

            //Diagonal
        else if (checkLine(0,4) && checkLine(4,8)) {
            return true;
        }
        else if (checkLine(2,4) && checkLine(4,6)) {
            return true;
        }
        else {
            return false;
        }


    }

    private boolean checkLine(int a, int b)
    {
        if (TicTacToeGUI.buttons[a].getText().equals(TicTacToeGUI.buttons[b].getText()) && !TicTacToeGUI.buttons[a].getText().equals("")) {
            log.trace("3 in a line");
            return true;
        }
        else {
            return false;
        }
    }

}


