package de.hdm_stuttgart.mi.sd;

import org.junit.Test;

import static org.junit.Assert.*;

public class TestTacToe {

    /**
     * Check if the flag is whether true or false depending on how many turns have been played
     */

    @Test
    public void testCheckButtons(){
        if (TicTacToeGUI.playersTurn == 9) {
            assertTrue(TicTacToeGUI.isFull);
        }
        if (TicTacToeGUI.playersTurn <= 9){
            assertFalse(TicTacToeGUI.isFull);
        }
    }

}