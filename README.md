# TicTacToe

## Introduction

> A classic Tic Tac Toe game with GUI made in Java Swing

## Detailed Function

Two players X and O place their marks in turns in a 3x3 grid field. The first player who as able to set 3 marks in horizontal, vertical or diagonal row, wins. 

## Screenshot

| <a href="https://gitlab.com/haidepzai/tic-tac-toe-gui" target="_blank">**Tic Tac Toe GUI**</a>


| [![Hai](https://i.ibb.co/SVqqrVP/Tic-Tac-Toe.jpg)](https://gitlab.com/haidepzai/tic-tac-toe-gui)